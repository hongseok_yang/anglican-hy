package anglican.analysis

//
// The following object contains general utility functions or values
//
object GUtil {
  def liftOption[T](l : List[Option[T]]) : Option[List[T]] = 
    if (l exists (_ == None)) None else Some(l map (_.get))

  def compareList[T](f:(T,T)=>Int, l1: List[T], l2: List[T]): Int = 
    (l1, l2) match {
      case (Nil, Nil) => 0
      case (Nil, _) => -1
      case (_, Nil) => 1
      case (x1::rest1, x2::rest2) => {
        val r = f(x1,x2)
        if (r != 0) r else compareList(f,rest1,rest2)
      }
    }

  def repeat[T](n: Int, x: T): List[T] = 
    (0 until n).toList map ((_) => x)
  def repeatedly[T](n: Int, f: Unit=>T): List[T] = 
    (0 until n).toList map ((_) => f(()))
}

private object NameGenerator {
  private val baseName = "__$$"
  private var count = 0
  private def genNameString(name: String): String = {
    count += 1
    baseName ++ "$" ++ name ++ count.toString
  }
  def genName(x: Name): Name = {
    val newName = genNameString(x.name)
    x.copy(name = newName)
  }
  def genName() : Name = {
    val newName = genNameString("")
    Name(newName)
  }
  def genName(s: String): Name = {
    val newName = genNameString(s)
    Name(newName)
  }
}

//
// The following object contains values and functions related to expressions.
// 
object EUtil {
  def genName(s: String): Name = NameGenerator.genName(s)
  def genName(x: Name): Name = NameGenerator.genName(x)
  def genName(): Name = NameGenerator.genName()

  private def eqList[A <: AnyRef](x: List[A], y: List[A]) : Boolean =
    (x.length == y.length) && ((x zip y) forall ((p) => p._1 eq p._2))
  private def eqListPair[A <: AnyRef, B <: AnyRef](x: List[(A,B)], y: List[(A,B)]) : Boolean =
    (x.length == y.length) && ((x zip y) forall ((p) => (p._1._1 eq p._2._1) && (p._1._2 eq p._2._2)))

  //
  // Types and routines for accessing information stored in annotated expressions
  //
  def getFreeVars(e: Expr): Set[Name] =
    e match {
      case EConst(_) => Set()
      case EVar(name) => Set(name)
      case ELam(name, args, body) => getFreeVars(body) -- (name :: args)
      case ELet(List(), body) => getFreeVars(body)
      case ELet((x,e)::bindings, body) => getFreeVars(ELet(bindings,body)) - x ++ getFreeVars(e) 
      case EApp(op, args) => getFreeVars(op::args)
      case EIf(cond, tbranch, fbranch) => getFreeVars(List(cond, tbranch, fbranch))
    }
  def getFreeVars(l: List[Expr]): Set[Name] =
    (l map getFreeVars).flatten.toSet

  //
  // Types and routines for performing capture-allowing renaming of variables
  //
  type RenameTy = Map[Name, Name]

  private def rename(x: Name, r: RenameTy): Name =
    r.getOrElse(x,x)

  def rename(e: Expr, r: RenameTy): Expr = 
    e match {
      case EConst(_) => 
        e
      case EVar(x) => 
        if (!r.contains(x)) e else EVar(r(x))
      case ELam(name, args, body) => {
        val newName = rename(name, r)
        val newArgs = args map (rename(_, r))
        val newBody = body map (rename(_, r))
        if ((name eq newName) && eqList(args, newArgs) && eqList(body, newBody)) e
        else ELam(newName, newArgs, newBody)
      }
      case ELet(bindings, body) => {
        def f(b : (Name, Expr)) = (rename(b._1,r), rename(b._2, r))
        val newBindings = bindings map f
        val newBody = body map (rename(_, r))
        if (eqListPair(bindings,newBindings) && eqList(body, newBody)) e
        else ELet(newBindings, newBody)
      }
      case EApp(op, args) => {
        val newOp = rename(op, r)
        val newArgs = args map (rename(_, r))
        if ((op eq newOp) && eqList(args, newArgs)) e
        else EApp(newOp, newArgs)
      }
      case EIf(c, e1, e2) => {
        val newC = rename(c, r)
        val newE1 = rename(e1, r)
        val newE2 = rename(e2, r)
        if ((c eq newC) && (e1 eq newE1) && (e2 eq newE2)) e
        else EIf(newC, newE1, newE2)
      }
    }

  //
  // Types and routines for performing capture-avoiding substitutions
  //
  type SubstTy = Map[Name, Expr]

  def getFreeVars(s: SubstTy): Set[Name] = {
    def f(vars: Set[Name], e: Expr) = vars ++ getFreeVars(e)
    s.values.foldLeft(Set():Set[Name])(f)
  }

  private def genRename(names: List[Name], forbiddenVars: Set[Name]): RenameTy = {
    def g(m: RenameTy, x: Name) = 
      if (!forbiddenVars.contains(x)) m else (m + (x -> genName(x)))
    names.foldLeft(Map():RenameTy)(g)
  }

  private def substLam(name: Name, args: List[Name], body: List[Expr], s: SubstTy) = {
    val boundVars = args.toSet + name
    val newS = s filter ((p) => (!boundVars.contains(p._1)))
    
    val freeVarsInNewS = getFreeVars(newS)
    val renaming = genRename((name :: args), freeVarsInNewS)

    if (renaming.isEmpty) {
      val newBody = body map (subst(_, newS))
      (name, args, newBody)
    }
    else {
      val newName::newArgs = (name::args) map (rename(_, renaming))
      val newBody = (body map (rename(_, renaming))) map (subst(_, newS))
      (newName, newArgs, newBody)
    }
  }

  private def substLet(
        accBindingsRev: List[(Name, Expr)],
        curBindings: List[(Name, Expr)], 
        curBody: List[Expr], 
        curS: SubstTy) : (List[(Name, Expr)], List[Expr]) = 
    curBindings match {
      case Nil => 
        (accBindingsRev.reverse, curBody map (subst(_, curS)))
      case (x,e)::bindingsRest => {
        val newE = subst(e, curS)
        val newS = curS filter ((p) => p._1 != x)

        val freeVarsInNewS = getFreeVars(newS)
        val renaming = genRename(List(x), freeVarsInNewS)

        if (renaming.isEmpty) {
          val newBindingsRev = (x,newE)::accBindingsRev
          substLet(newBindingsRev, bindingsRest, curBody, newS)
        }
        else {
          val newX = renaming.getOrElse(x,x)
          val newBindingsRev = (newX, newE)::accBindingsRev
          def g(b : (Name, Expr)) = (rename(b._1, renaming), rename(b._2, renaming)) 
          val newBindingsRest = bindingsRest map g
          val newBody = curBody map (rename(_, renaming))
          substLet(newBindingsRev, newBindingsRest, newBody, newS)
        }
      }
    }

  def subst(e: Expr, s: SubstTy): Expr = 
    e match {
      case EConst(_) => 
        e
      case EVar(x) => 
        if (s isDefinedAt x) s(x) else e
      case ELam(name, args, body) => {
        val (newName, newArgs, newBody) = substLam(name, args, body, s)
        if ((name eq newName) && eqList(args,newArgs) && eqList(body,newBody)) e
        else ELam(newName, newArgs, newBody)
      }
      case ELet(bindings, body) => {
        val (newBindings, newBody) = substLet(Nil, bindings, body, s)
        if (eqListPair(bindings, newBindings) && eqList(body,newBody)) e
        else ELet(newBindings, newBody)
      }
      case EApp(op, args) => {
        val newOp = subst(op, s)
        val newArgs = args map (subst(_, s))
        if ((op eq newOp) && eqList(args, newArgs)) e
        else EApp(newOp, newArgs)
      }
      case EIf(c, e1, e2) => {
        val newC = subst(c, s)
        val newE1 = subst(e1, s) 
        val newE2 = subst(e2, s)
        if ((newC eq c) && (newE1 eq e1) && (newE2 eq e2)) e
        else EIf(newC, newE1, newE2)
      }
    }

  val sOneD: Expr = EConst(CDouble(1.0))
  val sOneL: Expr = EConst(CLong(1))
  val sZeroD: Expr = EConst(CDouble(0.0))
  val sZeroL: Expr = EConst(CLong(0))
  val sTrue: Expr = EConst(CBool(true))
  val sFalse: Expr = EConst(CBool(false))
  val sNil: Expr = EConst(CNil)
  val sIdentityFn: Expr = ELam(Name("id"),List(Name("x")),List(EVar(Name("x"))))

  val sEqOp: Expr = EConst(CSymbol("="))
  val sAddOp: Expr = EConst(CSymbol("+"))
  val sMinusOp: Expr = EConst(CSymbol("-"))
  val sMulOp: Expr = EConst(CSymbol("*"))
  val sDivOp: Expr = EConst(CSymbol("/"))
  val sExpOp: Expr = EConst(CSymbol("exp"))
  val sLogOp: Expr = EConst(CSymbol("log"))
  val sLessOp: Expr = EConst(CSymbol("<"))
  val sLeqOp: Expr = EConst(CSymbol("<="))
  val sGreaterOp: Expr = EConst(CSymbol(">"))
  val sGeqOp: Expr = EConst(CSymbol(">="))
  val sAndOp: Expr = EConst(CSymbol("and"))
  val sOrOp: Expr = EConst(CSymbol("or"))
  val sNotOp: Expr = EConst(CSymbol("not"))
  val sIfOp: Expr = EConst(CSymbol("if"))
  val sWhenOp: Expr = EConst(CSymbol("when"))

  val sListOp: Expr = EConst(CSymbol("list"))
  val sVectorOp: Expr = EConst(CSymbol("vector"))
  val sSetOp: Expr = EConst(CSymbol("set"))
  val sConjOp: Expr = EConst(CSymbol("conj"))
  val sConcatOp: Expr = EConst(CSymbol("concat"))
  val sNthOp: Expr = EConst(CSymbol("nth"))
  val sCountOp: Expr = EConst(CSymbol("count"))
  val sRangeOp: Expr = EConst(CSymbol("range"))
  val sRepeatOp: Expr = EConst(CSymbol("repeat"))
  val sMapOp: Expr = EConst(CSymbol("map"))
  val sReduceOp: Expr = EConst(CSymbol("reduce"))
  val sRepeatedlyOp: Expr = EConst(CSymbol("repeatedly"))
  val sFilterOp: Expr = EConst(CSymbol("filter"))
  val sSomeOp: Expr = EConst(CSymbol("some"))
  val sCompOp: Expr = EConst(CSymbol("comp"))
  val sPartialOp: Expr = EConst(CSymbol("partial"))
  val sApplyOp: Expr = EConst(CSymbol("apply"))

  object CodeBool {
    // Destructor for code of constant integer
    def unapply(e: Expr): Option[Boolean] =
      e match { case EConst(CBool(b)) => Some(b); case _ => None }
    // Constructor for code of constant integer
    def apply(b: Boolean): Expr = if (b) sTrue else sFalse
  }
  object CodeKeyword {
    // Destructor for code of constant keyword
    def unapply(e: Expr): Option[String] = 
      e match { case EConst(CKeyword(s)) => Some(s); case _ => None }
    // Constructor for code of constant keyword
    def apply(s: String): Expr = EConst(CKeyword(s))
  }
  object CodeChar {
    // Destructor for code of constant character
    def unapply(e: Expr): Option[Char] =
      e match { case EConst(CChar(c)) => Some(c); case _ => None }
    // Constructor for code of constant character
    def apply(c: Char): Expr = EConst(CChar(c))
  }
  object CodeLong {
    // Destructor for code of constant integer
    def unapply(e: Expr): Option[Long] =
      e match { case EConst(CLong(l)) => Some(l); case _ => None }
    // Constructor for code of constant integer
    def apply(l: Long): Expr = EConst(CLong(l))
  }
  object CodeDouble {
    // Destructor for code of constant double number
    def unapply(e: Expr): Option[Double] =
      e match { case EConst(CDouble(d)) => Some(d); case _ => None }
    // Constructor for code of constant double number
    def apply(d: Double): Expr = EConst(CDouble(d)) 
  }
  object CodeSymbol {
    // Destructor for code of constant symbol
    def unapply(e: Expr): Option[String] =
      e match { case EConst(CSymbol(s)) => Some(s); case _ => None }
    // Constructor for code of constant symbol
    def apply(s: String): Expr = EConst(CSymbol(s)) 
  }
  object CodeString {
    // Destructor for code of constant string
    def unapply(e: Expr): Option[String] =
      e match { case EConst(CString(s)) => Some(s); case _ => None }
    // Constructor for code of constant string
    def apply(s: String): Expr = EConst(CString(s)) 
  }
  object CodeList {
    // Destructor for code of list
    def unapply(e: Expr): Option[List[Expr]] = 
      e match {
        case EApp(CodeSymbol("list"), l) => Some(l)
        case _ => None
      }
    // Constructor for code of list
    def apply(l: List[Expr]): Expr = EApp(sListOp, l)
  }
  object CodeVector {
    // Destructor for code of list
    def unapply(e: Expr): Option[List[Expr]] = 
      e match {
        case EApp(CodeSymbol("vector"), l) => Some(l)
        case _ => None
      }
    // Constructor for code of list
    def apply(l: List[Expr]): Expr = EApp(sVectorOp, l)
  }
  object CodeSet {
    // Destructor for code of list
    def unapply(e: Expr): Option[Set[Expr]] = 
      e match {
        case EApp(CodeSymbol("set"), s) => Some(s.toSet)
        case _ => None
      }
    // Constructor for code of list
    def apply(s: Set[Expr]): Expr = EApp(sSetOp, s.toList)
  }
  object CodeFun {
    private def typeErr[T](): T = {
      throw new IllegalArgumentException("Mismatch in the numbers of formal and actual arguments [CodeFun]")
    }
    // Destructor for code of list
    def unapply(e: Expr): Option[List[Expr]=>Expr] = {
      e match {
        case CodeSymbol(_) => {
          def resMetaF(actualArgs: List[Expr]): Expr = EApp(e, actualArgs)
          Some(resMetaF)
        }
        case EApp(CodeSymbol("comp"), Nil) => {
          def resMetaF(actualArgs: List[Expr]): Expr = 
            actualArgs match { case List(e0) => e0; case _ => typeErr() }
          Some(resMetaF)
        }
        case EApp(CodeSymbol("comp"), fs) => {
          val metaFsOpt = GUtil.liftOption(fs map unapply)
          def applyF(mF: List[Expr]=>Expr, e0: Expr): Expr = mF(List(e0))
          def combineFs(mFs: List[List[Expr]=>Expr]): Option[List[Expr]=>Expr] = {
            val lastF = mFs.last
            val restFs = mFs.dropRight(1)
            def metaF(actualArgs: List[Expr]): Expr = restFs.foldRight(lastF(actualArgs))(applyF)
            Some(metaF)
          }
          metaFsOpt flatMap combineFs
        }
        case EApp(CodeSymbol("partial"), f::args) => {
          val metaFOpt = unapply(f)
          def adaptF(mF: List[Expr]=>Expr): Option[List[Expr]=>Expr] = 
            Some((actualArgs) => mF(args ++ actualArgs))
          metaFOpt flatMap adaptF
        }
        case ELam(_, formalArgs, body) => {
          def metaF(actualArgs: List[Expr]): Expr = {
            if (actualArgs.length != formalArgs.length) typeErr()
            else {
              val sub = (formalArgs zip actualArgs).toMap
              (body map (subst(_,sub))) match {
                case Nil => sNil
                case List(b) => b
                case newBody => ELet(Nil,newBody)
              }
            }
          }
          Some(metaF)
        }
        case _ => None
      }
    }
  }

  def isConst(e: Expr) =
    e match { 
      case EConst(_) => true
      case _ => false 
    }
  def isFalseInClojure(e: Expr) =
    e match { 
      case CodeBool(false) | EConst(CNil) => true 
      case _ => false 
    }
  def isTrueInClojure(e: Expr) = 
    e match { 
      case CodeBool(true) | CodeDouble(_) | CodeLong(_) 
         | CodeKeyword(_) | CodeChar(_) | CodeString(_) | CodeSymbol(_) => true 
      case _ => false
    }
}

