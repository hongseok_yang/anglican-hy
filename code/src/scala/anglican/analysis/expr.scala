package anglican.analysis

//
// Data type for constants
//
sealed trait ConstValue extends Ordered[ConstValue] {
  def compare(that: ConstValue) =
    (this, that) match {
      case (CNil, CNil) => 0
      case (CNil, _) => -1
      case (_, CNil) => 1
      case (CBool(b1), CBool(b2)) => b1 compare b2
      case (CBool(_), _) => -1
      case (_, CBool(_)) => 1
      case (CChar(c1), CChar(c2)) => c1 compare c2
      case (CChar(_), _) => -1
      case (_, CChar(_)) => 1
      case (CDouble(d1), CDouble(d2)) => d1 compare d2
      case (CDouble(_), _) => -1
      case (_, CDouble(_)) => 1
      case (CKeyword(k1), CKeyword(k2)) => k1 compare k2
      case (CKeyword(_), _) => -1
      case (_, CKeyword(_)) => 1
      case (CLong(l1), CLong(l2)) => l1 compare l2
      case (CLong(_), _) => -1
      case (_, CLong(_)) => 1
      case (CString(s1), CString(s2)) => s1 compare s2
      case (CString(_), _) => -1
      case (_, CString(_)) => 1
      case (CSymbol(s1), CSymbol(s2)) => s1 compare s2
    }
}
case object CNil extends ConstValue
case class CBool(value: Boolean) extends ConstValue
case class CChar(value: Char)  extends ConstValue
case class CDouble(value: Double) extends ConstValue
case class CKeyword(value: String) extends ConstValue
case class CLong(value: Long) extends ConstValue
case class CString(value: String) extends ConstValue
case class CSymbol(value: String) extends ConstValue

//
// Name is a class for variables. It stores the name and other
// useful information of a variable. 
//
case class Info() extends Ordered[Info] {
  def compare(that: Info) = 0
}
object DefaultInfo extends Info()
case class Name(name: String, info: Info = DefaultInfo) extends Ordered[Name] {
  def compare(that: Name) = (name, info) compare (that.name, that.info)
}

//
// Data type for expressions
//
sealed trait Expr extends Ordered[Expr] {
  private def compareList[T](f: (T,T)=>Int, l1: List[T], l2: List[T]): Int = 
    (l1, l2) match {
      case (Nil, Nil) => 0
      case (Nil, _) => -1
      case (_, Nil) => 1
      case (x1::rest1, x2::rest2) => {
        val r = f(x1,x2)
        if (r != 0) r else compareList(f,rest1,rest2)
      }
    }
  private def compareExprList(es1: List[Expr], es2: List[Expr]): Int = {
    def f(e1: Expr, e2: Expr): Int = e1 compare e2
    compareList(f, es1, es2)
  }
  private def compareNameList(ns1: List[Name], ns2: List[Name]): Int = {
    def f(n1: Name, n2: Name): Int = n1 compare n2
    compareList(f, ns1, ns2)
  }
  private def compareNameExprList(l1: List[(Name,Expr)], l2: List[(Name,Expr)]): Int = {
    def f(p1: (Name, Expr), p2: (Name, Expr)) = {
      val r = p1._1 compare p2._1
      if (r != 0) r else (p1._2 compare p2._2)
    }
    compareList(f, l1, l2)
  }
  def compare(that: Expr): Int =
    (this, that) match {
      case (EApp(op1, args1), EApp(op2, args2)) => {
        val r = op1 compare op2
        if (r != 0) r else compareExprList(args1, args2)
      }
      case (EApp(_,_), _) => -1
      case (_, EApp(_,_)) => 1
      case (EConst(c1), EConst(c2)) => 
        c1 compare c2
      case (EConst(_), _) => -1
      case (_, EConst(_)) => 1
      case (EIf(c1,t1,f1), EIf(c2,t2,f2)) => {
        val r1 = c1 compare c2 
        if (r1 != 0) r1 
        else { 
          val r2 = t1 compare t2
          if (r2 != 0) r2 else f1 compare f2
        }
      }
      case (EIf(_,_,_), _) => -1
      case (_, EIf(_,_,_)) => 1
      case (ELam(n1, args1, body1), ELam(n2, args2, body2)) => {
        val r1 = n1 compare n2
        if (r1 != 0) r1
        else { 
          val r2 = compareNameList(args1, args2)
          if (r2 != 0) r2 else compareExprList(body1, body2)
        }
      }
      case (ELam(_, _, _), _) => -1
      case (_, ELam(_, _, _)) => 1
      case (ELet(bindings1, body1), ELet(bindings2, body2)) => {
        val r1 = compareNameExprList(bindings1, bindings2)
        if (r1 != 0) r1 else compareExprList(body1, body2)
      }
      case (ELet(_, _), _) => -1
      case (_, ELet(_, _)) => 1
      case (EVar(n1), EVar(n2)) => n1 compare n2
    }
}
case class EApp(op: Expr, args: List[Expr]) extends Expr
case class EConst(value: ConstValue) extends Expr
case class EIf(cond: Expr, tbranch: Expr, fbranch: Expr) extends Expr
case class ELam(name: Name, args: List[Name], body: List[Expr]) extends Expr
case class ELet(bindings: List[(Name,Expr)], body: List[Expr]) extends Expr
case class EVar(name: Name) extends Expr 
