package anglican.analysis

import scala.collection.immutable.SortedMap

import GUtil.{liftOption,compareList}
import EUtil.{subst,getFreeVars,genName}
import EUtil.{sNil}

import Prover.checkInconsistency
import Prover.{sAdd,sMinus,sMul,sDiv,sLog,sExp}
import Prover.{sAnd,sOr,sNot}
import Prover.{sEq,sLeq,sLess,sGeq,sGreater}
import Prover.{sList,sVector,sSet,sConj,sConcat,sNth,sCount,sRange,sRepeat,sSome,sComp,sPartial}
import Prover.{sMap,sReduce,sFilter,sRepeatedly,sApply}

import Dist.{getSupport,getSupportExpr,getLogScore,getLogScoreExpr}
import Dist.{isFiniteDist,isInfiniteDist,isCompositeDist}

object SymExec {
  private def isEffectFreeOp(op: Expr): Boolean = 
    op match {
      case EConst(CSymbol(opStr)) => 
        (opStr != "sample") && (opStr != "observe") && (opStr != "predict")
      case _ => 
        false
    }

  // A generic trait that specifies the interface of a symbolic executor.
  // It takes a type T of results of symbolic execution. 
  //
  // To someone familiar with monads: T is the type obtained by the
  // application M[Expr] for some monad M. The unit and bind operations
  // are the usual monad operations. However, handleIf and HandleApp are
  // not generic monad operations. They are specific to our implementation.
  trait ResultBuilder[T] {
    def unit(e: Expr): T 
    def bind(c1: T, c2: Expr=>T): T
    def handleIf(cond: T, tbranch: T, fbranch: T): T
    def handleApp(op: T, args: List[T], evaluator: Expr => T): T

    def bindList(cList: List[T], continuation: List[Expr]=>T): T = 
      cList match {
        case Nil => 
          continuation(Nil)
        case c::cList1 => 
          bind(c, (v)=>bindList(cList1, ((l)=>continuation(v::l))))
      }
  }

  //
  // Main data structure for recording results of a probabilistic symbolic executor
  // 
  case class PResult(
    // value -- the value of a given expression computed by symbolic execution.
    value: Expr, 
    // obs -- list of observe statements encountered during symbolic execution. 
    obs: List[(Expr,Expr)] = List(),
    // rvs -- map from random variables to their distributions 
    rvs: SortedMap[Name,Expr] = SortedMap(),
    // score -- log score 
    score: Expr = EConst(CDouble(0.0)),
    // constraint -- conjunction of path constraints
    constraint: Expr = EConst(CBool(true))
  ) extends Ordered[PResult] {
    def joinAnd(those: List[PResult]): PResult = {
      val newScore = sAdd(score :: (those map (_.score)))
      val newObs = (those map (_.obs)).foldLeft(obs)(_ ++ _)
      val newRvs = (those map (_.rvs)).foldLeft(rvs)(_ ++ _)
      val newConstraint = sAnd(constraint :: (those map (_.constraint)))
      this.copy(rvs = newRvs, obs = newObs, score = newScore, constraint = newConstraint)
    }
    def joinOr(those: List[PResult]): PResult = {
      val allScores = score :: (those map (_.score))
      // println("[joinOr] allScores: " + allScores)
      val expAllScores = allScores map sExp
      val newScore = sLog(sAdd(expAllScores))
      val newRvs = (those map (_.rvs)).foldLeft(rvs)(_ ++ _)
      // println("[joinOr] newScore: " + newScore)
      val newConstraint = sOr(constraint :: (those map (_.constraint)))
      this.copy(rvs = newRvs, score = newScore, constraint = newConstraint)
    }
    def joinOr(that: PResult): PResult = joinOr(List(that))

    def compare(that: PResult) = {
      def f(e1: (Expr,Expr), e2: (Expr,Expr)) = e1 compare e2 
      def g(ne1: (Name,Expr), ne2: (Name,Expr)) = ne1 compare ne2
      val r1 = (value, constraint, score) compare (that.value, that.constraint, that.score)
      if (r1 != 0) r1 
      else {
        val r2 = compareList(f, obs, that.obs)
        if (r2 != 0) r2 else compareList(g, rvs.toList, that.rvs.toList)
      }
    }
  }
  private val emptyPResult = PResult(sNil)
  private def joinNoDisjPResults(l: List[PResult]): List[PResult] = {
    val l_grouped = l groupBy ((r)=> (r.value, r.obs, r.constraint))
    val ll = l_grouped.toList map (_._2) 
    ll map ((l0) => l0.head joinOr l0.tail)
  }
  private def joinDisjPResults(l1: List[PResult], l2: List[PResult]): List[PResult] = {
    def isLess(r1: PResult, r2: PResult): Boolean = 
      ((r1.value < r2.value) || (r1.value == r2.value && r1.constraint < r2.constraint))
    val l1_sorted = l1 sortWith isLess
    val l2_sorted = l2 sortWith isLess 
    def join(acc: List[PResult], rs1: List[PResult], rs2: List[PResult]): List[PResult] = 
      (rs1, rs2) match { 
        case (Nil, Nil) => acc.reverse
        case (Nil, _) => (acc ++ rs2).reverse
        case (_, Nil) => (acc ++ rs1).reverse
        case (r1::rs1rest, r2::rs2rest) => 
          if (r1 == r2 && r1.obs == r2.obs)
            join((r1 joinOr r2)::acc, rs1rest, rs2rest)
          else if (r1.obs != r2.obs || r1 < r2) 
            join(r1::acc, rs1rest, rs2)
          else 
            join(r2::acc, rs1, rs2rest)
      }
    join(Nil, l1_sorted, l2_sorted)
  }

  //
  // Implementation of a probabilistic symbolic executor
  // 
  class PResultBuilder extends ResultBuilder[Option[List[PResult]]] {
    type T = Option[List[PResult]]

    private def clean(l: List[PResult]): List[PResult] = {
      val l1 = l filter ((r:PResult) => !checkInconsistency(r.constraint))
      joinNoDisjPResults(l1)
    }
    def unit(e: Expr): T = 
      Some(List(emptyPResult.copy(value = e)))
    def bind(c1: T, c2: Expr=>T): T = {
      def f(r1: PResult): T = {
        for (resultFromC2 <- c2(r1.value)) 
          yield (resultFromC2 map ((r2)=>r2.joinAnd(List(r1))))
      }
      for (l <- c1; results <- GUtil.liftOption(l map f))
        yield clean(results.flatten)
    }
    def handleIf(cond: T, tbranch: T, fbranch: T): T = {
      def conjoin(c: Expr, result: T): T = {
        for(l <- result) yield {
          for(r <- l; c1 = sAnd(List(r.constraint, c)); if !checkInconsistency(c1)) 
            yield r.copy(constraint = c1)
        }
      }
      def continuation(c: Expr): T = {
        for (tResult <- conjoin(c, tbranch); fResult <- conjoin(sNot(c), fbranch)) 
          yield joinDisjPResults(tResult,fResult)
      }
      bind(cond, continuation)
    }

    private val opMapVal: Map[String,List[Expr]=>Expr] = 
      Map("+" -> (sAdd _), "-" -> (sMinus _), "*" -> (sMul _), "/" -> (sDiv _),
        "log" -> (sLog _), "exp" -> (sExp _), "and" -> (sAnd _), "or" -> (sOr _),
        "not" -> (sNot _), "=" -> (sEq _), "<=" -> (sLeq _), "<" -> (sLess _),
        ">=" -> (sGeq _), ">" -> (sGreater _), 
        "list" -> (sList _), "vector" -> (sVector _), "set" -> (sSet _),
        "conj" -> (sConj _), "concat" -> (sConcat _), "nth" -> (sNth _), "count" -> (sCount _), 
        "range" -> (sRange _), "repeat" -> (sRepeat _), "some" -> (sSome _), "comp" -> (sComp _),
        "partial" -> (sPartial _))
    private val opMapCom: Map[String,List[Expr]=>(Boolean,Expr)] = 
      Map("map" -> (sMap _), "reduce" -> (sReduce _), "filter" -> (sFilter _), 
        "repeatedly" -> (sRepeatedly _), "apply" -> (sApply _))

    private def sampleFromDist(distE: Expr): T = {
      def allocateToRenv(): T = {
        val rv = genName("R")
        val rvMap = SortedMap(rv->distE)
        Some(List(PResult(value=EVar(rv), rvs=rvMap)))
      }
      def enumerate(values: List[Expr], computeScore: Expr => Option[Expr]): T = 
        for(scores <- liftOption(values map computeScore))
          yield {
            ((values zip scores) map ((vs) => PResult(vs._1, score = vs._2)))
          }
      def allocateOrEnumerate(support: Option[List[Expr]], computeScore: Expr => Option[Expr]): T = {
        support match {
          case None => allocateToRenv()
          case Some(values) => enumerate(values, computeScore)
        }
      }
      distE match {
        case EApp(EConst(CSymbol(dist)), args) if isFiniteDist(dist) => {
          def computeScore(v: Expr): Option[Expr] = getLogScore(dist, v, args) 
          val support = getSupport(dist, args) 
          allocateOrEnumerate(support, computeScore)
        }
        case EApp(EConst(CSymbol(dist)), args) if isInfiniteDist(dist) => 
          allocateToRenv()
        case EApp(EConst(CSymbol(dist)), args) if isCompositeDist(dist) => {
          def computeScore(v: Expr): Option[Expr] = getLogScoreExpr(distE, v)
          val support = getSupportExpr(distE)
          allocateOrEnumerate(support, computeScore) 
        }
        case _ => None
      }
    }
    def handleApp(op: T, args: List[T], evaluator: Expr => T): T = {
      def continuation(opV: Expr, argsV: List[Expr]): T = {
        (opV, argsV) match {
          case (EConst(CSymbol("sample")), List(distE)) =>
            sampleFromDist(distE)
          case (EConst(CSymbol("observe")), List(distE,obsV)) => 
            Some(List(PResult(value=sNil, obs=List((distE,obsV)))))
          case (EConst(CSymbol(op)), _) if opMapVal.isDefinedAt(op) => 
            unit(opMapVal(op)(argsV))
          case (EConst(CSymbol(op)), _) if opMapCom.isDefinedAt(op) => 
            opMapCom(op)(argsV) match {
              case (true, e) => evaluator(e)
              case (false, e) => unit(e)
            }
          case (ELam(name, vars, body), _) if vars.length == argsV.length => {
            val sub = (vars zip argsV).toMap
            val newBody = body map (subst(_, sub))
            evaluator(ELet(Nil, newBody))
          }
          case (_, _) if isEffectFreeOp(opV) =>
            unit(EApp(opV, argsV))
          case (_, _) => 
            None
        }
      }
      bind(op, (o)=>(bindList(args, continuation(o,_))))
    }
  }

  //
  // Type for environments mapping variables to values computed by symbolic evaulation
  // 
  type EnvTy = Map[Name, Expr]

  //
  // A generic routine that evaluates an annotated expression ei under 
  // the environment env using a result builder rb.
  // 
  private def _evaluate[T](rb: ResultBuilder[T], env: EnvTy, e: Expr) : T = {
    e match {
      case EConst(c) => rb.unit(e)
      case EVar(x) => rb.unit(env.getOrElse(x,e)) 
      case ELam(fname, args, body) => {
        val boundNames = args.toSet + fname
        val newEnv = env filter ((p) => !boundNames.contains(p._1))
        val newBody = body map (subst(_, newEnv)) 
        val newExpr = ELam(fname, args, newBody)
        rb.unit(newExpr)
      }
      case ELet(Nil, body) => {
        val resultsFromBody: List[T] = body map (_evaluate(rb, env, _))
        def continuation(l: List[Expr]): T = 
          l match { 
            case Nil => rb.unit(sNil)
            case _ => rb.unit(l.last)
          }
        rb.bindList(resultsFromBody, continuation)
        // val resultE = rb.bindList(resultsFromBody, continuation)
        // println("[_evaluate] resultsFromBody: " + resultsFromBody)
        // println("[_evaluate] resultE: " + resultE)
        // resultE
      }
      case ELet((x0,e0)::bindings, body) => {
        val resultFromE0 = _evaluate(rb, env, e0)
        val newEnv = env filter ((p) => p._1 != x0)
        val newE = ELet(bindings, body)
        def continuation(newE0: Expr) = _evaluate(rb, newEnv + (x0 -> newE0), newE)
        rb.bind(resultFromE0, continuation)
      }
      case EApp(op, args) => {
        val resultFromOp = _evaluate(rb, env, op)
        val resultsFromArgs = args map (_evaluate(rb, env, _))
        rb.handleApp(resultFromOp, resultsFromArgs, _evaluate(rb, env, _))
      }
      case EIf(cond, tbranch, fbranch) => {
        val resultFromCond = _evaluate(rb, env, cond)
        val resultFromTBranch = _evaluate(rb, env, tbranch)
        val resultFromFBranch = _evaluate(rb, env, fbranch)
        rb.handleIf(resultFromCond, resultFromTBranch, resultFromFBranch)
      }
    }
  }

  type TestResultTy = (Expr, List[(Expr,Expr)], List[(Expr,Expr)], Expr, Expr)
  def run(shouldPrint: Boolean, e: Expr) : Option[List[TestResultTy]] = {
    val rb = new PResultBuilder
    val env: EnvTy = Map()
    val result = _evaluate(rb, env, e) 
    val resultSorted = result match { case None => None; case Some(l) => Some(l.sorted) }
    if (shouldPrint) { println("=== Input to Symbolic Execution ==="); println(e) }
    if (shouldPrint) { println("=== Results of Symbolic Execution ==="); println(resultSorted) }
    def convertRvs(rvs: SortedMap[Name,Expr]): List[(Expr,Expr)] = 
      rvs.toList map ((rvDef) => (EVar(rvDef._1), rvDef._2)) 
    for (l <- resultSorted) yield {
      (l map ((r) => (r.value, r.obs, convertRvs(r.rvs), r.score, r.constraint)))
    }
  }
  def run(e: Expr) : Option[List[TestResultTy]] = run(true, e)
}

