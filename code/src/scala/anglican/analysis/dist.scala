package anglican.analysis

import java.util.Date
import cern.jet.random.Binomial
import cern.jet.random.engine.DRand

import GUtil.liftOption
import EUtil.{sOneD,sZeroD,sTrue,sFalse}
import EUtil.{CodeLong,CodeDouble,CodeList,CodeVector,CodeSymbol}
import EUtil.isConst

import Prover.{sAdd,sMinus,sMul,sDiv,sLog}
import Prover.{sEq,sIf}

object Dist {
  private val RNG = new DRand(new Date)

  private def checkVectorThen[T](e: Expr, f: List[Expr] => Option[T]): Option[T] =
    e match { 
      case CodeVector(l) => f(l)
      case _ => None 
    }
  private def checkBinaryVectorThenFirst(e: Expr): Option[Expr] = {
    def f(es: List[Expr]): Option[Expr] = 
      es match { case List(e1,_) => Some(e1); case _ => None }
    checkVectorThen(e, f)
  }
  private def checkBinaryVectorThenSecond(e: Expr): Option[Expr] = {
    def f(es: List[Expr]): Option[Expr] = 
      es match { case List(_,e2) => Some(e2); case _ => None }
    checkVectorThen(e, f)
  }
  private def checkBinaryVectorThenPair(e: Expr): Option[(Expr,Expr)] = {
    def f(es: List[Expr]): Option[(Expr,Expr)] = 
      es match { case List(e1,e2) => Some((e1,e2)); case _ => None }
    checkVectorThen(e, f)
  }

  private val finiteDists = 
    Set("bernoulli", "binomial", "categorical", "discrete", "flip", "uniform-discrete")
  private val infiniteDiscreteDists = 
    Set("negative-binomial", "poisson")
  private val infiniteContinuousDists = 
    Set("beta", "exponential", "gamma", "normal", "uniform-continuous")
  private val infiniteDists = 
    infiniteDiscreteDists ++ infiniteContinuousDists
  private val compositeDists = 
    Set("product-dist", "product-iid")
  private val dists = 
    finiteDists ++ infiniteDists ++ compositeDists

  def isFiniteDist(dist: String): Boolean = finiteDists.contains(dist)
  def isInfiniteDist(dist: String): Boolean = infiniteDists.contains(dist)
  def isCompositeDist(dist: String): Boolean = compositeDists.contains(dist)

  private def getSupportForRange(min: Int, max: Int): List[Expr] = {
    val range = (min until max).toList
    val support = range map (CodeLong(_))
    support
  }

  def getSupport(dist: String, args: List[Expr]): Option[List[Expr]] = {
    def handleDiscrete(l: List[Expr]) = 
      Some(getSupportForRange(0, l.length))
    def handleCategorical(l: List[Expr]) = 
      liftOption(l map checkBinaryVectorThenFirst) 
    (dist, args) match {
      case (("bernoulli" | "flip"), _) =>
        Some(List(sTrue, sFalse))
      case ("binomial", List(_,CodeLong(max))) =>
        Some(getSupportForRange(0, max.toInt))
      case ("categorical", List(CodeList(l))) =>
        handleCategorical(l)
      case ("categorical", List(CodeVector(l))) =>
        handleCategorical(l)
      case ("discrete", List(CodeList(l))) =>
        handleDiscrete(l)
      case ("discrete", List(CodeVector(l))) =>
        handleDiscrete(l)
      case ("uniform-discrete", List(CodeLong(min),CodeLong(max))) =>
        Some(getSupportForRange(min.toInt, max.toInt))
      case _ => 
        None
    }
  }


  def getSupportExpr(e: Expr): Option[List[Expr]] = {
    e match {
      case EApp(CodeSymbol(dist), args) if (!isCompositeDist(dist)) => 
        getSupport(dist, args)
      case EApp(CodeSymbol("product-iid"), List(dist1, CodeLong(n2))) => {
        /*
        def combine(values: List[Expr]) = {
          val valuesCopied = (0 to n2).toList map (_ => values) 
          None
        }
        for(values <- getSupportAllDist(dist1, args1)) yield { combine(values) }
        */
        None
      }
      case EApp(CodeSymbol("product-dist"), args1) => {
        None
      }
    }
  }

  private def getLogScoreForCategorical(value: Expr, valueProbs: List[(Expr,Expr)]): Expr = {
    valueProbs match {
      case Nil =>
        throw new IllegalArgumentException("Wrong number of arguments [getLogScoreForCategorical]")
      case List((v,_)) => 
        if (isConst(v) && isConst(value) && (v != value))
          throw new IllegalArgumentException("Value outside of support [getLogScoreForCategorical]")
        else
          sZeroD
      case _ => {
        val values = valueProbs map (_._1)
        val normalisingConst = sAdd(valueProbs map (_._2))
        val normalisedProbs = valueProbs map ((vp)=> sDiv(List(vp._2, normalisingConst)))
        val scores = normalisedProbs map (sLog _)
        val (vn,probn)::valueProbsRev = (values zip scores).reverse
        val score = valueProbsRev.foldLeft(probn)((acc,vp) => sIf(sEq(value,vp._1),vp._2,acc))
        score
      }
    }
  }

  private def getLogScoreForDiscrete(value: Expr, probs: List[Expr]): Expr = {
    val values = getSupportForRange(0, probs.length)
    val valueProbs = (values zip probs)
    getLogScoreForCategorical(value, valueProbs)
  }

  private def getLogScoreForBinomial(n: Int, prob: Expr, max: Int): Expr = {
    val logTwo = CodeDouble(Math.log(2.0))
    val logProb = sLog(prob)
    val logOneMinusProb = sLog(sMinus(sOneD,prob))
    val dist = new Binomial(max, 0.5, RNG)
    val e0 = CodeDouble(Math.log(dist.pdf(n.toInt)))
    val e1 = sMul(List(CodeDouble(max.toDouble), logTwo))
    val e2 = sMul(List(CodeDouble(n.toDouble), logProb))
    val e3 = sMul(List(CodeDouble((max-n).toDouble), logOneMinusProb))
    sAdd(List(e0,e1,e2,e3))
  }

  def getLogScore(dist: String, value: Expr, args: List[Expr]): Option[Expr] = {
    def handleBinomial(prob: Expr, max: Int): Option[Expr] = {
      value match {
        case CodeLong(n) => Some(getLogScoreForBinomial(n.toInt, prob, max))
        case _ => None
      }
    }
    def handleCategorical(valueProbs: List[Expr]): Option[Expr] = {
      for(valueProbs0 <- liftOption(valueProbs map checkBinaryVectorThenPair))
        yield getLogScoreForCategorical(value, valueProbs0)
    }
    (dist, args) match {
      case (("flip" | "bernoulli"), List(prob)) => {
        val scoreTrue = sLog(prob)
        val scoreFalse = sLog(sMinus(sOneD,prob))
        Some(sIf(value, scoreTrue, scoreFalse))
      }
      case ("binomial", List(prob, CodeLong(max))) =>
        handleBinomial(prob, max.toInt) 
      case ("categorical", List(CodeList(valueProbs))) => 
        handleCategorical(valueProbs)
      case ("categorical", List(CodeList(valueProbs))) => 
        handleCategorical(valueProbs)
      case ("discrete", List(CodeList(probs))) => 
        Some(getLogScoreForDiscrete(value, probs))
      case ("discrete", List(CodeVector(probs))) => 
        Some(getLogScoreForDiscrete(value, probs))
      case ("uniform-discrete", List(min, max)) => {
        val sizeOfSupport = sMinus(max,min)
        Some(sMinus(sLog(sizeOfSupport)))
      }
      case _ =>  None
    }
  }
  
  def getLogScoreExpr(distE: Expr, value: Expr): Option[Expr] = 
    None
}
