package anglican.analysis

import Math.{exp,log,PI}

import GUtil.{liftOption,repeat,repeatedly}

import EUtil.{getFreeVars,subst,genName}

import EUtil.{sOneD,sOneL,sZeroD,sZeroL,sTrue,sFalse,sNil,sIdentityFn}
import EUtil.{sEqOp,sAddOp,sMinusOp,sMulOp,sDivOp,sExpOp,sLogOp}
import EUtil.{sLessOp,sLeqOp,sGreaterOp,sGeqOp,sAndOp,sOrOp,sNotOp,sIfOp,sWhenOp}
import EUtil.{sListOp,sVectorOp,sSetOp,sConjOp,sConcatOp,sNthOp,sCountOp,sRangeOp}
import EUtil.{sRepeatOp,sMapOp,sReduceOp,sRepeatedlyOp}
import EUtil.{sFilterOp,sSomeOp,sCompOp,sPartialOp,sApplyOp}

import EUtil.{CodeBool,CodeKeyword,CodeChar,CodeLong,CodeDouble,CodeSymbol,CodeString}
import EUtil.{CodeList,CodeVector,CodeSet,CodeFun}
import EUtil.{isConst,isTrueInClojure,isFalseInClojure}

object Prover {
  private def sPrimOp(op: Expr, args: List[Expr]): Expr = 
    EApp(op, args)
  private def sPrimOp(op: Expr, arg: Expr): Expr = 
    sPrimOp(op, List(arg))


  private def flattenOp(opName: String, e: Expr) = 
    e match {
      case EApp(CodeSymbol(opName1), argsOfE) if opName == opName1 => argsOfE 
      case _ => List(e)
    }

  private def unConstDouble(e: Expr) =
    (e: @unchecked) match { case CodeDouble(v) => v }
  private def unConstLong(e: Expr) = 
    (e: @unchecked) match { case CodeLong(v) => v }
  private def isNumber(e: Expr) = 
    e match { 
      case CodeDouble(_) => 0
      case CodeLong(_) => 1
      case _ => 2
    }

  private object ConstN {
    def unapply(e: Expr): Option[ConstValue] = 
      e match {
        case EConst(c @ (CLong(_) | CDouble(_))) => Some(c)
        case _ => None
      }
  }
  private def evalConstN(
    opL: (Long,Long) => Expr,
    opD: (Double,Double) => Expr,
    c1: ConstValue, c2: ConstValue): Expr = 
  {
    (c1, c2) match {
      case (CLong(l1), CLong(l2)) => opL(l1,l2)
      case (CDouble(d1), CLong(l2)) => opD(d1,l2.toDouble)
      case (CLong(l1), CDouble(d2)) => opD(l1.toDouble,d2)
      case (CDouble(d1), CDouble(d2)) => opD(d1,d2)
      case _ => throw new IllegalArgumentException(c1 +" and "+ c2 +" should be numbers")
    }
  }
  private def sBoolBOp(
    op: Expr,
    opL: (Long, Long) => Boolean,
    opD: (Double, Double) => Boolean,
    e1: Expr, e2: Expr): Expr = 
  {
    def opL1(l1: Long, l2: Long) = CodeBool(opL(l1, l2))
    def opD1(d1: Double, d2: Double) = CodeBool(opD(d1, d2))
    (e1, e2) match {
      case (ConstN(c1), ConstN(c2)) => evalConstN(opL1, opD1, c1, c2)
      case _ => sPrimOp(op,List(e1,e2))
    }
  }
  private def sNumBOp(
    op: Expr,
    opL: (Long, Long) => Long,
    opD: (Double, Double) => Double,
    e1: Expr, e2: Expr): Expr = 
  {
    def opL1(l1: Long, l2: Long) = CodeLong(opL(l1, l2))
    def opD1(d1: Double, d2: Double) = CodeDouble(opD(d1, d2))
    (e1, e2) match {
      case (ConstN(c1), ConstN(c2)) => evalConstN(opL1, opD1, c1, c2)
      case _ => sPrimOp(op,List(e1,e2))
    }
  }
  private def sNumBOp(
    op: Expr,
    opD: (Double, Double) => Double,
    e1: Expr, e2: Expr): Expr = 
  {
    def opL1(l1: Long, l2: Long) = CodeDouble(opD(l1.toDouble, l2.toDouble))
    def opD1(d1: Double, d2: Double) = CodeDouble(opD(d1, d2))
    (e1, e2) match {
      case (ConstN(c1), ConstN(c2)) => evalConstN(opL1, opD1, c1, c2)
      case _ => sPrimOp(op,List(e1,e2))
    }
  }

  private case class GroupOnNum(
    op: Expr, opName: String, opInvName: String, 
    unitLE: Expr, unitDE: Expr,
    opL: (Long,Long) => Long, unitL: Long,
    opD: (Double,Double) => Double, unitD: Double
  )
  private def cancel(
    opInv: String,
    e: Expr, 
    esSeen: List[Expr], 
    esUnseen: List[Expr]): (List[Expr], List[Expr]) = 
  {
    esUnseen match {
      case Nil => 
        (List(e), esSeen.reverse)
      case EApp(CodeSymbol(op1), List(e1,e2)) :: es1 if op1 == opInv && e2 == e => 
        (Nil, esSeen.reverse ++ (e1 :: es1))
      case e1 :: es1 =>
        cancel(opInv, e, e1::esSeen, es1)
    }
  }
  private def cancelInverses(opInv: String, acc: List[Expr], es: List[Expr]): List[Expr] = 
    es match { 
      case Nil => acc
      case e::es0 => {
        cancel(opInv, e, Nil, es0) match {
          case (Nil, _) => 
            cancelInverses(opInv, acc, es0)
          case (List(e), _) => {
            val (newEs, newAcc) = cancel(opInv, e, Nil, acc) 
            cancelInverses(opInv, newEs ++ newAcc, es0)
          }
          case _ => 
            throw new IllegalArgumentException("Impossible pattern [cancelInverses]")
        }
      }
    }
  private def sGroupOp(g: GroupOnNum, args: List[Expr]): Expr = {
    val args0 = args.foldLeft(Nil:List[Expr])(_ ++ flattenOp(g.opName, _))
    val args1 = cancelInverses(g.opInvName, List(), args0)
    // println("[sGroupOp] args0: " + args0)
    // println("[sGroupOp] args1: " + args1)
    val argsGrouped = args1 groupBy isNumber
    val argsD = argsGrouped.getOrElse(0,List())
    val argsL = argsGrouped.getOrElse(1,List())
    val argsNonum = argsGrouped.getOrElse(2,List())
    val totalD0 = argsD.foldLeft(g.unitD)((acc,e)=>g.opD(acc, unConstDouble(e)))
    val totalL0 = argsL.foldLeft(g.unitL)((acc,e)=>g.opL(acc, unConstLong(e)))
    val newArgs = 
      if ((totalD0 == g.unitD) && (totalL0 == g.unitL))
        argsNonum
      else if (totalD0 == g.unitD)
        CodeLong(totalL0) :: argsNonum
      else 
        CodeDouble(g.opD(totalD0, totalL0.toDouble)) :: argsNonum
    newArgs match {
      case Nil => if (argsD.length == 0) g.unitLE else g.unitDE
      case List(e) => e
      case _ => sPrimOp(g.op, newArgs)
    }
  }

  private val groupForAdd = 
    GroupOnNum(
      op = sAddOp, opName = "+", opInvName = "-", 
      unitLE = sZeroL, unitDE = sZeroD, 
      opL = (_ + _), unitL = (0: Long),
      opD = (_ + _), unitD = (0.0: Double)
  )
  private val groupForMul =
    GroupOnNum(
      op = sMulOp, opName = "*", opInvName = "/", 
      unitLE = sOneL, unitDE = sOneD,
      opL = (_ * _), unitL = (1: Long),
      opD = (_ * _), unitD = (1.0: Double)
  )
  def sAdd(args: List[Expr]): Expr = sGroupOp(groupForAdd, args)
  def sMul(args: List[Expr]): Expr = sGroupOp(groupForMul, args)

  def sMinus(e: Expr): Expr =
    e match {
      case CodeLong(l) => CodeLong(-l)
      case CodeDouble(d) => CodeDouble(-d)
      case EApp(CodeSymbol("-"), List(e0)) => e0
      case _ => sPrimOp(sMinusOp, List(e))
    }
  def sMinus(e1: Expr, e2: Expr): Expr = 
    (e1, e2) match {
      case (_, CodeLong(0)) | (_, CodeDouble(0.0)) => e1
      case (CodeLong(0), _) | (CodeDouble(0.0), _) => sMinus(e1)
      case _ => sNumBOp(sMinusOp, (_ - _), (_ - _), e1, e2)
    }
  def sMinus(es: List[Expr]): Expr = 
    es match { 
      case List(e) => sMinus(e)
      case List(e1,e2) => sMinus(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sMinus]")
    }

  def sDiv(e1: Expr, e2: Expr): Expr =
    (e1, e2) match {
      case (_, CodeLong(0)) | (_, CodeDouble(0.0)) =>
        throw new IllegalArgumentException("Divide-by-zero error")
      case (CodeLong(0), _) | (CodeDouble(0.0), _) => 
        sZeroD
      case _ => 
        sNumBOp(sDivOp, (_ / _), e1, e2)
    }
  def sDiv(es: List[Expr]): Expr = 
    es match { 
      case List(e1,e2) => sDiv(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sDiv]")
    }

  def sExp(e: Expr): Expr = 
    e match { 
      case CodeDouble(d) => CodeDouble(exp(d))
      case CodeLong(l) => CodeDouble(exp(l))
      case EApp(CodeSymbol("log"), List(e)) => e
      case EApp(CodeSymbol("-"), List(CodeDouble(0.0), e1)) => 
        sPrimOp(sDivOp, List(sOneD, sPrimOp(sExpOp, e1)))  
      case EApp(CodeSymbol("-"), List(CodeLong(0), e1)) => 
        sPrimOp(sDivOp, List(sOneD, sPrimOp(sExpOp, e1)))  
      case _ => sPrimOp(sExpOp,List(e))
    }
  def sExp(es: List[Expr]): Expr =
    es match { 
      case List(e) => sExp(e)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sExp]")
    }

  def sLog(e: Expr): Expr = 
    e match {
      case CodeDouble(d) => CodeDouble(log(d))
      case CodeLong(l) => CodeDouble(log(l))
      case EApp(CodeSymbol("exp"), List(e1)) => e1
      case EApp(CodeSymbol("/"), List(CodeDouble(1.0), e1)) => 
        sPrimOp(sMinusOp, List(sZeroD, sPrimOp(sLogOp, e1)))  
      case EApp(CodeSymbol("/"), List(CodeLong(1), e1)) => 
        sPrimOp(sMinusOp, List(sZeroL, sPrimOp(sLogOp, e1)))  
      case _ => sPrimOp(sLogOp,e)
    }
  def sLog(es: List[Expr]): Expr =
    es match { 
      case List(e) => sLog(e)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sLog]")
    }

  def sAnd(args: List[Expr]): Expr = {
    val args0 = args.foldLeft(Nil:List[Expr])(_ ++ flattenOp("and", _))
    val args1 = args0 filter ((e) => !isTrueInClojure(e))
    def f(acc: List[Expr], e:Expr) = 
      if (acc exists (checkImplication(_,e))) acc 
      else e :: (acc filter (!checkImplication(e,_)))
    val args2 = args1.foldLeft(Nil:List[Expr])(f)
    // println("[sAnd] IN: " + args + ";; MID: " + args1 + ";; OUT: " + args2)
    if (args2 exists isFalseInClojure) 
      sFalse
    else if (args2.isEmpty) 
      sTrue
    else if (args2.length == 1)
      args2.head
    else 
      sPrimOp(sAndOp, args2) 
  }

  def sOr(args: List[Expr]): Expr = {
    val args0 = args.foldLeft(Nil:List[Expr])(_ ++ flattenOp("or", _))
    val args1 = args0 filter ((e) => !isFalseInClojure(e))
    def f(acc: List[Expr], e:Expr) = if (acc exists (_ == e)) acc else (e::acc)
    val args2 = args1.foldLeft(Nil:List[Expr])(f)
    // println("[sOr] IN: " + args + "; OUT: " + args2)
    if (args2 exists isTrueInClojure)
      sTrue
    else if (args2.isEmpty) 
      sFalse
    else if (args2.length == 1)
      args2.head
    else 
      sPrimOp(sOrOp, args2) 
  }

  def sNot(e: Expr): Expr = {
    if (isFalseInClojure(e)) return sTrue
    if (isTrueInClojure(e)) return sFalse
    e match { 
      case EApp(CodeSymbol(opN), List(e1,e2)) => {
        val m = Map(">" -> "<=", "<=" -> ">",
                    "<" -> ">=", ">=" -> "<",
                    "=" -> "not=", "not=" -> "=")
        m.lift(opN) match {
          case None => sPrimOp(sNotOp, List(e))
          case Some(opN1) => EApp(CodeSymbol(opN1), List(e1,e2)) 
        }
      }
      case _ => sPrimOp(sNotOp, List(e))
    }
  }
  def sNot(es: List[Expr]): Expr = 
    es match { 
      case List(e) => sNot(e)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sNot]")
    }


  def sIf(c: Expr, e1: Expr, e2: Expr): Expr = {
    if (isFalseInClojure(c)) return e2
    if (isTrueInClojure(c)) return e1
    if (e1 == e2) return e1
    EApp(sIfOp, List(c,e1,e2))
  }

  def sWhen(c: Expr, e: Expr): Expr = {
    if (isFalseInClojure(c)) return sNil
    if (isTrueInClojure(c)) return e
    EApp(sWhenOp, List(c,e))
  }  

  def sEq(e1: Expr, e2: Expr): Expr = 
    if (e1 == e2) sTrue 
    else {
      (e1, e2) match {
        case (EConst(c1), EConst(c2)) => if (c1 == c2) sTrue else sFalse
        case _ => sPrimOp(sEqOp, List(e1, e2))
      }
    }
  def sEq(es: List[Expr]): Expr =
    es match { 
      case List(e1,e2) => sEq(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sEq]")
    }

  def sLeq(e1: Expr, e2: Expr): Expr = 
    sBoolBOp(sLeqOp, (_ <= _), (_ <= _), e1, e2)
  def sLeq(es: List[Expr]): Expr = 
    es match { 
      case List(e1,e2) => sLeq(e1,e2) 
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sLeq]")
    }

  def sLess(e1: Expr, e2: Expr): Expr = 
    sBoolBOp(sLessOp, (_ < _), (_ < _), e1, e2)
  def sLess(es: List[Expr]): Expr = 
    es match { 
      case List(e1,e2) => sLess(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sLess]")
    }

  def sGeq(e1: Expr, e2: Expr): Expr = 
    sBoolBOp(sGeqOp, (_ >= _), (_ >= _), e1, e2)
  def sGeq(es: List[Expr]): Expr = 
    es match { 
      case List(e1,e2) => sGeq(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sGeq]")
    }

  def sGreater(e1: Expr, e2: Expr): Expr = 
    sBoolBOp(sGreaterOp, (_ > _), (_ > _), e1, e2)
  def sGreater(es: List[Expr]): Expr = 
    es match { 
      case List(e1,e2) => sGreater(e1,e2)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sGreater]")
    }


  def sList(es: List[Expr]) = CodeList(es)
  def sVector(es: List[Expr]) = CodeVector(es)
  def sSet(es: List[Expr]) = CodeSet(es.toSet)

  def sConj(es: List[Expr]): Expr =
    es match {
      case CodeList(l1) :: es2 => CodeList(es2.foldLeft(l1)((acc0,e0)=>(e0::acc0)))
      case CodeVector(v1) :: es2 => CodeVector(v1 ++ es2)
      case CodeSet(s1) :: es => CodeSet(s1 ++ es)
      case _ => sPrimOp(sConjOp, es)
    }
  def sConcat(es: List[Expr]): Expr = {
    def f(e: Expr, accO:Option[List[Expr]]): Option[List[Expr]] = 
      (accO, e) match { 
        case (None, _) => None
        case (Some(acc), CodeList(l)) => Some(l ++ acc)
        case (Some(acc), CodeVector(l)) => Some(l ++ acc)
        case _ => None
      }
    es.foldRight(Some(Nil):Option[List[Expr]])(f) match {
      case None => sPrimOp(sConcatOp, es)
      case Some(l) => CodeList(l)
    }
  }  

  def sNth(es: List[Expr]): Expr = 
    es match {
      case List(CodeList(l1), CodeLong(d1)) => 
        if (0 <= d1 && d1 < l1.length) l1(d1.toInt)
        else throw new IllegalArgumentException("Out-of-index error [sNth]")
      case List(CodeVector(l1), CodeLong(d1)) => 
        if (0 <= d1 && d1 < l1.length) l1(d1.toInt)
        else throw new IllegalArgumentException("Out-of-index error [sNth]")
      case List(_, _) => 
        sPrimOp(sNthOp, es)
      case _ => 
        throw new IllegalArgumentException("Wrong number of arguments [sNth]")
    }

  def sCount(es: List[Expr]): Expr =
    es match {
      case List(CodeList(l)) => CodeLong(l.length)
      case List(CodeVector(l)) => CodeLong(l.length)
      case List(CodeSet(s)) => if (s forall isConst) CodeLong(s.size) else sPrimOp(sCountOp, es)
      case List(_) => sPrimOp(sCountOp, es)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sCount]")
    }

  def sRange(es: List[Expr]): Expr =
    es match {
      case List(CodeLong(l1), CodeLong(l2)) => {
        val range = (l1 until l2).toList 
        val rangeCode = range map (CodeLong(_))
        CodeList(rangeCode)
      }
      case List(_, _) => 
        sPrimOp(sRangeOp, es)
      case _ => 
        throw new IllegalArgumentException("Wrong number of arguments [sRange]")
    }

  def sRepeat(es: List[Expr]): Expr =
    es match {
      case List(CodeLong(l1), e) => CodeList(repeat(l1.toInt, e))
      case List(_, _) => sPrimOp(sRepeatOp, es)
      case _ => throw new IllegalArgumentException("Wrong number of arguments [sRepeat]")
    }

  def sSome(es: List[Expr]): Expr = sPrimOp(sSomeOp, es)
  def sPartial(es: List[Expr]): Expr = sPrimOp(sPartialOp, es)
  def sComp(es: List[Expr]): Expr = sPrimOp(sPartialOp, es)

  // The returned boolean indicates whether the returned expression should be evaluated further.
  def sMap(es: List[Expr]): (Boolean,Expr) = {
    es match {
      case Nil | List(_) => 
        throw new IllegalArgumentException("Wrong number of arguments [sMap]")
      case CodeFun(metaF) :: esRest => {
        def unListOrVector(e: Expr): Option[List[Expr]] = {
          e match { 
            case CodeList(l) => Some(l)
            case CodeVector(l) => Some(l) 
            case _ => None 
          }
        }
        def transpose(ll: List[List[Expr]]) = {
          if (ll == Nil) throw new IllegalArgumentException("Impossible case [sMap]")
          val llRev = ll.reverse
          val llRevRest: List[List[Expr]] = llRev.tail
          val baseRes: List[List[Expr]] = llRev.head map (List(_))
          def f(acc: List[List[Expr]], l: List[Expr]) = (l zip acc) map ((p)=>(p._1 :: p._2))
          llRevRest.foldLeft(baseRes)(f)
        }
        liftOption(esRest map unListOrVector) match {
          case None => (false, sPrimOp(sMapOp, es))
          case Some(es1) => {
            val actualArgsList = transpose(es1)
            (true, CodeList(actualArgsList map metaF))
          }
        }
      }
      case _ => (false, sPrimOp(sMapOp, es))
    }
  }

  // The returned boolean indicates whether the returned expression should be evaluated further.
  def sReduce(es: List[Expr]): (Boolean,Expr) = {
    es match {
      case Nil | List(_) => 
        throw new IllegalArgumentException("Wrong number of arguments [sReduce]")
      case CodeFun(metaF)::esRest => {
        def getBaseList(es0: List[Expr]): Option[(Expr, List[Expr])] = {
          es0 match {
            case List(base, CodeList(l)) => 
              Some((base, l))
            case List(base, CodeVector(l)) => 
              Some((base, l))
            case List(base, CodeSet(s)) if (s forall isConst) => 
              Some((base, s.toList))
            case List(CodeList(base::l)) => 
              Some((base, l))
            case List(CodeVector(base::l)) => 
              Some((base, l))
            case List(CodeSet(s)) if (s forall isConst) && (s.size > 0) => {
              val l = s.toList
              Some((l.head, l.tail))
            }
            case _ => None
          }
        }
        def f(acc0: Expr, e0: Expr) = metaF(List(acc0, e0))
        getBaseList(esRest) match {
          case None => (false, sPrimOp(sReduceOp, es))
          case Some((base, l)) => (true, l.foldLeft(base)(f))
        }
      }
      case _ => (false, sPrimOp(sReduceOp, es))
    }
  }

  // The returned boolean indicates whether the returned expression should be evaluated further.
  def sRepeatedly(es: List[Expr]): (Boolean,Expr) = 
    es match {
      case List(CodeLong(l), CodeFun(metaF)) => (true, CodeList(repeatedly(l.toInt, ((_:Unit)=>metaF(Nil)))))
      case _ => (false, sPrimOp(sRepeatedlyOp, es))
    }

  // The returned boolean indicates whether the returned expression should be evaluated further.
  def sFilter(es: List[Expr]): (Boolean,Expr) = 
    es match {
      case Nil | List(_) => 
        throw new IllegalArgumentException("Wrong number of arguments [sFilter]")
      case List(CodeFun(metaF), e) => {
        def formSelection(e0: Expr) = {
          sIf(metaF(List(e0)), CodeList(List(e0)), CodeList(Nil))
        }
        def unListOrVectorOrSet(e0: Expr): Option[List[Expr]] = {
          e0 match { 
            case CodeList(l) => Some(l)
            case CodeVector(l) => Some(l)
            case CodeSet(s) if (s forall isConst) => Some(s.toList)
            case _ => None
          }
        }
        unListOrVectorOrSet(e) match {
          case None => (false, sPrimOp(sFilterOp, es))
          case Some(l) => (true, CodeList(l map formSelection))
        }
      }
      case _ => (false, sPrimOp(sFilterOp, es))
    }

  // The returned boolean indicates whether the returned expression should be evaluated further.
  def sApply(es: List[Expr]): (Boolean,Expr) = 
    es match {
      case Nil | List(_) =>
        throw new IllegalArgumentException("Wrong number of arguments [sApply]")
      case _ => {
        val lastE = es.last
        val restEs = es.dropRight(1)
        lastE match {
          case CodeList(l) => (true, EApp(restEs.head, restEs ++ l)) 
          case CodeVector(l) => (true, EApp(restEs.head, restEs ++ l)) 
          case CodeSet(s) if (s forall isConst) => (true, EApp(restEs.head, restEs ++ s.toList)) 
          case _ => (false, sPrimOp(sApplyOp, es))
        }
      }
    }

  // Checks whether the conjunction of the input is inconsistent.
  // If it returns true, this means that the conjunction implies false.
  // Otherwise, we cannot make any conclusion. 
  def checkInconsistency(es: List[Expr]): Boolean = {
    def getSubst(sub0: Map[Name,Expr], es0: List[Expr]): Map[Name,Expr] = {
      es0 match { 
        case Nil => sub0
        case EApp(CodeSymbol("="), List(e0 @ EVar(name0), e1 @ EVar(name1))) :: es0rest => {
          val sub1: Map[Name,Expr] = if (name0 < name1) Map(name1->e0) 
                                     else if (name0 > name1) Map(name0->e1) 
                                     else Map()
          val sub2 = (sub0 mapValues (subst(_, sub1))) ++ sub1
          val es1 = es0rest map (subst(_, sub2))
          getSubst(sub2, es1)
        }
        case EApp(CodeSymbol("="), List(EVar(name0), e0)) :: es0rest => {
          val sub1 = Map(name0->e0)
          val sub2 = (sub0 mapValues (subst(_, sub1))) ++ sub1
          val es1 = es0rest map (subst(_, sub2))
          getSubst(sub2, es1)
        }
        case EApp(CodeSymbol("="), List(e0, EVar(name0))) :: es0rest => {
          val sub1 = Map(name0->e0)
          val sub2 = (sub0 mapValues (subst(_, sub1))) ++ sub1
          val es1 = es0rest map (subst(_, sub2))
          getSubst(sub1, es1)
        }
        case _ :: es0rest => getSubst(sub0, es0rest)
      }
    }
    def checkSimpleInconsistency(e0: Expr): Boolean = {
      if (isFalseInClojure(e0)) return true
      e0 match {
        case EApp(CodeSymbol("="), List(e1, e2)) => 
          isConst(e1) && isConst(e2) && (e1 != e2)
        case EApp(CodeSymbol("not="), List(e1, e2)) => 
          (e1 == e2)
        case _ => 
          false
      }
    }
    val sub = getSubst(Map(): Map[Name,Expr],  es)
    val es1 = es map (subst(_, sub))
    es1 exists checkSimpleInconsistency
  }
  def checkInconsistency(e: Expr) : Boolean = checkInconsistency(List(e))

  // Checks whether e1 implies e2. The result "true" means that e1 implies e2.
  // "false" means that we do not know.
  def checkImplication(e1: Expr, e2: Expr) : Boolean = checkInconsistency(List(e1, sNot(e2)))
}
