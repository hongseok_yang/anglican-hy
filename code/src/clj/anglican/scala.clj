(ns anglican.scala
  "All the functions needed to connect the Clojure implementation with the Scala implementation"
  (:require [t6.from-scala.core :refer [$ $$] :as $])
  (:import [scala.collection.immutable List])
  (:import [anglican.analysis
            SymExec$
            ConstValue CLong CDouble CKeyword CChar CString CSymbol CNil$ CBool
            Info Name DefaultInfo$
            Expr EConst EVar ELam ELet EApp EIf Info]))


(defn seq-to-scala-list
  "Convert a Clojure sequence to a Scala list"
  [s]
  (let [s0 (seq s)]
    (if (nil? s0) 
      ($ List/empty)
      (reduce #($ %1 "::" %2) ($ List/empty) (reverse s0)))))

(defn scala-list-to-seq
  "Convert a Scala list to a Clojure sequence"
  [obj]
  (.foldRight obj '() ($/fn [x acc] (conj acc x))))

(defn vector-to-scala-pair
  "Convert a Scala pair to a Clojure vector"
  [[x y]]
  ($/tuple x y))

(defn scala-pair-to-vector
  "Convert a Scala pair to a Clojure vector"
  [obj]
  [(._1 obj) (._2 obj)])

(defn convert-symb-to-str
  "Convert a symbol to a string"
  [s]
  (if (symbol? s)
    (str s)
    (assert false (str "Illegal input. " s " is not a symbol"))))

(defn convert-str-to-symb
  "Convert a string to a symbol"
  [s]
  (if (string? s)
    (symbol s)
    (assert false (str "Illegal input. " s " is not a string"))))

(defn convert-symb-to-name
  "Convert a symbol to a name"
  [s]
  (if (symbol? s)
    (Name. (str s) DefaultInfo$/MODULE$)
    (assert false (str "Illegal input. " s " is not a symbol"))))

(defn convert-name-to-symb
  "Convert a name to a symbol"
  [n]
  (if (instance? Name n)
    (symbol (.name n))
    (assert false (str "Illegal input. " n " is not a name"))))

(def constant-doubles
  {'Math/PI Math/PI})

(defn constant?
  "Check whether an expression is a constant or not."
  [vs expr]
  ; vs is a set of bound variables.
  (let [constants #{'sample 'observe 
                    'bernoulli 'binomial 'categorical 'discrete 'flip 'uniform-discrete
                    'negative-binomial 'poisson
                    'beta 'exponential 'gamma 'normal 'uniform-continuous 
                    'product-dist 'product-iid
                    '+ '- '* '/ 'exp 'log 
                    'and 'or 'not
                    '= '<= '< '>= '> 
                    'vector 'list 'set 
                    'concat 'conj
                    'nth 'count 'range 'repeat 
                    'map 'reduce 'filter 'some 'repeatedly 'comp 'partial 
                    'conditional}]
    (and 
      (not (contains? vs expr))
      (or
        (contains? constant-doubles expr)
        (number? expr)
        (keyword? expr)
        (char? expr)
        (string? expr)
        (true? expr)
        (false? expr)
        (nil? expr)
        (contains? constants expr)))))

(defn variable?
  "Check whether an expression is a variable or not"
  [vs expr]
  (or
    (contains? vs expr)
    (and (not (constant? vs expr)) (symbol? expr))))

(declare to-scala)

(defn to-scala-const-value
  "Convert a constant to a Scala ConstValue object"
  [expr]
  (cond
    (contains? constant-doubles expr) (CDouble. (constant-doubles expr))
    (float? expr)                     (CDouble. expr)
    (integer? expr)                   (CLong. expr)
    (float? expr)                     (CDouble. expr)
    (keyword? expr)                   (CKeyword. (str expr))
    (char? expr)                      (CChar. expr)
    (string? expr)                    (CString. expr)
    (symbol? expr)                    (CSymbol. (str expr))
    (true? expr)                      (CBool. true)
    (false? expr)                     (CBool. false)
    (nil? expr)                       (CNil$/MODULE$)
    :else (assert false (str "Cannot convert " expr " to a Scala ConstValue object"))))

(defn to-scala-const
  "Convert a constant to a Scala Expr object"
  [expr]
  (EConst. (to-scala-const-value expr)))

(defn to-scala-var
  "Convert a variable to a Scala Expr object"
  [expr]
  (EVar. (convert-symb-to-name expr)))

(defn to-scala-vector
  "Convert a literal vector to a Scala Expr object"
  [vs expr]
  (to-scala vs `(~'vector ~@expr)))

(defn to-scala-hash-map
  "Convert a literal hash map to a Scala Expr object"
  [vs expr]
  (to-scala vs `(~'hash-map ~@(apply concat (seq expr)))))

(defn to-scala-set
  "Convert a literal set to a Scala Expr object"
  [vs expr]
  (to-scala vs `(~'set (~'list ~@expr))))

(defn to-scala-fn
  "Convert a fn expression to a Scala Expr object"
  [vs args]
  (if (vector? (first args))
    (to-scala-fn vs `[nil ~@args])
    (let [[name params & body] args]
      (let [new-name  (convert-symb-to-name (symbol (str name)))
            new-args  (seq-to-scala-list (map convert-symb-to-name params))
            new-vs    (apply conj vs params)
            new-body  (seq-to-scala-list (map (partial to-scala new-vs) body))]
        (ELam. new-name new-args new-body)))))

(defn to-scala-loop 
  "Convert a loop expression to a Scala Expr object"
  [vs [bindings & body]]
  (let [f-args (take-nth 2 bindings)
        a-args (take-nth 2 (rest bindings))
        f      `(~'fn ~'loop [~@f-args] ~@body)]
    (to-scala vs `(~f ~@a-args))))

(defn to-scala-do 
  "Convert a do expression to a Scala Expr object"
  [vs body]
  (let [new-body (seq-to-scala-list (map (partial to-scala vs) body))]
    (ELet. ($ List/empty) new-body)))
                             
(defn to-scala-let 
  "Convert a let expression to a Scala Expr object"
  [vs args]
  (let [[bindings & body] args]
    (if (empty? bindings)
      (ELet. ($ List/empty) 
             (seq-to-scala-list (map (partial to-scala vs) body)))
      (let [bindings-grouped      (partition 2 bindings)
            handle-binding        (fn [[cur-bindings cur-vs] [x e]]
                                    [(conj cur-bindings 
                                           [(convert-symb-to-name x) (to-scala cur-vs e)])
                                     (conj cur-vs x)])
            [new-bindings new-vs] (reduce handle-binding
                                          [[] vs]
                                          bindings-grouped)
            res-bindings          (seq-to-scala-list
                                    (map (partial apply $/tuple) new-bindings)) 
            res-body              (seq-to-scala-list 
                                    (map (partial to-scala new-vs) body))]
        (ELet. res-bindings res-body)))))

(defn to-scala-if
  "Convert an if expression to a Scala Expr object"
  [vs args]
  (let [new-args (map (partial to-scala vs) args)
        new-cond (first new-args)
        new-then (second new-args)
        new-else (if (= (count args) 3)  
                   (nth new-args 2) 
                   (to-scala-const 'nil))]
    (EIf. new-cond new-then new-else)))

(defn to-scala-cond
  "Convert a cond expression to a Scala Expr object"
  [vs args]
  (if-not (seq args)
    (to-scala-const 'nil)
    (let [[c e & args-rest] args]
      (if (= c :else)
        (to-scala vs e)
        (let [new-rest `(~'cond ~@args-rest)
              new-args (list c e new-rest)]
          (to-scala-if vs new-args)))))) 

(defn to-scala-app
  "Convert an application to a Scala Expr object" 
  [vs oper args]
  (let [new-args (seq-to-scala-list (map (partial to-scala vs) args))
        new-oper (to-scala vs oper)]
    (EApp. new-oper new-args)))

(defn to-scala
  "Convert an Anglican expression in Clojure to a data structure in Scala"
  [vs expr] 
  ; vs is a set of variables bound by the context. 
  ; Initially, it is empty but grows as recusion progresses.
  (cond
    (variable? vs expr)  (to-scala-var expr)
    (constant? vs expr)  (to-scala-const expr)
    (vector? expr)       (to-scala-vector vs expr)
    (map? expr)          (to-scala-hash-map vs expr)
    (set? expr)          (to-scala-set vs expr)
    (seq? expr)          (let [[kwd & args] expr]
                           (case kwd
                             fn            (to-scala-fn vs args)
                             loop          (to-scala-loop vs args)
                             let           (to-scala-let vs args)
                             if            (to-scala-if vs args)
                             when          (to-scala-if vs args)
                             cond          (to-scala-cond vs args)
                             do            (to-scala-do vs args)
                             (to-scala-app vs kwd args)))
    :else (assert false (str "Cannot convert " expr " to a Scala data structure"))))

(declare from-scala)

(defn from-scala-const
  "Convert a Scala EConst object to a constant in Anglican/Clojure"
  [obj]
  (let [v (.value obj)]
    (cond 
      (instance? CLong v)      (.value v)
      (instance? CDouble v)    (.value v)
      (instance? CKeyword v)   (keyword (subs (.value v) 1))
      (instance? CChar v)      (.value v)
      (instance? CString v)    (.value v)
      (instance? CSymbol v)    (symbol (.value v))
      (instance? CBool v)      (.value v)
      (= v CNil$/MODULE$)      'nil
      :else (assert false (str "Cannot convert " obj " to a constant expression in Anglican/Clojure")))))

(defn from-scala-var
  "Convert a Scala EVar object to a variable in Anglican/Clojure"
  [obj]
  (convert-name-to-symb (.name obj)))

(defn from-scala-lam
  "Convert a Scala ELam object to a fn expression in Anglican/Clojure"
  [obj]
  (let [fn-name0  (convert-name-to-symb (.name obj))
        fn-name   (if (= fn-name0 (str nil)) nil fn-name0)
        fn-args   (scala-list-to-seq (.args obj))
        fn-body   (scala-list-to-seq (.body obj))
        new-args  (map convert-name-to-symb fn-args)
        new-body  (map from-scala fn-body)]
    `(~'fn ~fn-name [~@new-args] ~@new-body)))

(defn from-scala-let
  "Convert a Scala Elet object to a let expression in Anglican/Clojure"
  [obj]
  (let [bindings     (scala-list-to-seq (.bindings obj))
        body         (scala-list-to-seq (.body obj))
        convert-pair (fn [acc p] 
                       (conj acc 
                             (convert-name-to-symb (._1 p)) 
                             (from-scala (._2 p))))
        new-bindings (reduce convert-pair [] bindings)
        new-body     (map from-scala body)]
    `(~'let [~@new-bindings] ~@new-body)))

(defn from-scala-app
  "Convert a Scala EApp object to a function application in Anglican/Clojure"
  [obj]
  (let [oper     (.op obj)
        args     (scala-list-to-seq (.args obj))
        new-oper (from-scala oper)
        new-args (map from-scala args)]
    `(~new-oper ~@new-args)))

(defn from-scala-if
  "Convert a Scala MIf object to an if expression in Anglican/Clojure"
  [obj]
  (let [c       (from-scala (.cond obj))
        tbranch (from-scala (.tbranch obj))
        fbranch (from-scala (.fbranch obj))]
    `(~'if ~c ~tbranch ~fbranch)))

(defn from-scala
  "Convert a Scala Expr object to an Anglican expression in Clojure"
  [obj]
  (cond
    (instance? EConst obj)  (from-scala-const obj)
    (instance? EVar obj)    (from-scala-var obj)
    (instance? ELam obj)    (from-scala-lam obj)
    (instance? ELet obj)    (from-scala-let obj)
    (instance? EApp obj)    (from-scala-app obj)
    (instance? EIf obj)     (from-scala-if obj)
    :else (assert false (str "Cannot convert " obj " to an expression in Anglican/Clojure"))))
