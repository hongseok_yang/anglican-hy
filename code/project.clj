(defproject anglican "0.7.1-SNAPSHOT"
  :description "Anglican, a probabilistic programming system"
  :url "http://bitbucket.org/dtolpin/anglican"
  :license {:name "GNU General Public License Version 3"
            :url "http://www.gnu.org/licenses/gpl.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 [org.clojure/data.json "0.2.6"]
                 [com.taoensso/timbre "4.0.2"]
                 [colt "1.2.0"]
                 [net.mikera/core.matrix "0.36.1"]
                 [net.mikera/core.matrix.stats "0.6.0"]
                 [net.mikera/vectorz-clj "0.30.1"]
                 [t6/from-scala "0.3.0-SNAPSHOT"]
                 [org.scala-lang/scala-library "2.11.7"]]
  :plugins [[codox "0.8.11"] 
            [lein-zinc "0.1.4"]]
  :min-lein-version "2.5.0"
  :scm {:name "git"
        :url "https://bitbucket.org/dtolpin/anglican"}
  :repl-options {:timeout 600000}
  :main ^:skip-aot anglican.core
  :target-path "target/"
  :source-paths ["src/clj"]
  :resource-paths ["../examples"]
  :profiles {:uberjar {:aot :all}}
  :deploy-branches ["master" "development"]
  :prep-tasks ["zinc"]
  :aliases {"publish" ["do" ["clean"] ["test"] ["uberjar"]]})
