(ns anglican.symexec-test
  (:require [clojure.test :refer [deftest testing is]])
  (:require [t6.from-scala.core :refer [$ $$] :as $])
  (:import [anglican.analysis SymExec$])
  (:import [Math.log])
  (:use anglican.scala)
  (:use anglican.runtime))

(defn- run-sub
  [vs code f]
  (let [code0 (to-scala vs code)]
    ($/if-let [code1 (.run SymExec$/MODULE$ false code0)]
      (map f (scala-list-to-seq code1))
      nil)))

(defn- compare-result-sub
  [result1 result2 f]
  (cond
    (and (= result1 nil) (= result2 nil)) 
      true
    (and (not= result1 nil) (not= result2 nil))
      (and (= (count result1) (count result2)) 
           (every? true? (map f result1 result2)))
    :else
      false))

(defn- is-close? 
  [n1 n2] 
  (let [d (- n1 n2)] (and (< -0.05 d) (< d 0.05))))

(defn- const-evaluate 
  [const env vs e]
;  (println "[const-evaluate]")
;  (println "[const-evaluate] const: " const)
;  (println "[const-evaluate] env: " env)
;  (println "[const-evaluate] vs: " vs)
;  (println "[const-evaluate] e: " e)
  (let [bindings (interleave vs (repeat (count vs) const))
        closed-e `(~'let [~'normal normal 
                          ~'flip flip 
                          ~'observe observe 
                          ~'log log
                          ~'nth nth
                          ~'list list
                          ~@env ~@bindings] ~e)
        evaluated-e (eval closed-e)]
;    (println "[const-evaluate] bindings: " bindings)
;    (println "[const-evaluate] closed-e: " closed-e)
;    (println "[const-evaluate] evaluated-e: " evaluated-e)
    evaluated-e))

(defn- const-evaluate-obs
  [const env vs obs]
  (let [f (fn [[dist v]] (const-evaluate const env vs `(observe ~dist ~v)))]
    (sort-by str (map f obs))))

(defn- const-evaluate-renv
  [const env vs renv]
  (let [f (fn [dist] (const-evaluate const env vs `(observe ~dist ~const)))]
    (sort-by str (map (comp f second) renv))))

(defn- const-evaluate-vrp
  [const env [v renv s & others]]
  (let [vs (map first renv)]
    (into
      [(const-evaluate const env vs v)
       (const-evaluate-renv const env vs renv)
       (const-evaluate const env vs s)]
      others)))

(defn- const-evaluate-vorp
  [const env [v obs renv s & others]]
  (let [vs (map first renv)]
    (into
      [(const-evaluate const env vs v)
       (const-evaluate-obs const env vs obs)
       (const-evaluate-renv const env vs renv)
       (const-evaluate const env vs s)]
      others)))

(defn- const-evaluate-vrp-result
  [const env results]
  (map (partial const-evaluate-vrp const env) results))

(defn- const-evaluate-vorp-result
  [const env results]
  (map (partial const-evaluate-vorp const env) results))

(defn- run
  [vs code]
  (let [f (fn [x] [(from-scala (._1 x)) (from-scala (._4 x))])]
    (run-sub vs code f)))

(defn- vrp-run
  [vs code]
  (let [f (fn [pair] [(from-scala (._1 pair)) (from-scala (._2 pair))])
        g (fn [renv] (map f (scala-list-to-seq renv))) 
        h (fn [x] [(from-scala (._1 x)) (g (._3 x)) (from-scala (._4 x))])]
    (run-sub vs code h)))

(defn- vorp-run
  [vs code]
  (let [f (fn [pair] [(from-scala (._1 pair)) (from-scala (._2 pair))])
        g (fn [renv] (map f (scala-list-to-seq renv))) 
        h (fn [x] [(from-scala (._1 x)) (g (._2 x)) (g (._3 x)) (from-scala (._4 x))])]
    (run-sub vs code h)))

(defn- vorpc-run
  [vs code]
  (let [f (fn [pair] [(from-scala (._1 pair)) (from-scala (._2 pair))])
        g (fn [renv] (map f (scala-list-to-seq renv))) 
        h (fn [x] [(from-scala (._1 x)) 
                   (g (._2 x)) 
                   (g (._3 x)) 
                   (from-scala (._4 x)) 
                   (from-scala (._5 x))])]
    (run-sub vs code h)))

(defn- compare-result
  [result1 result2]
  (let [result1s (sort-by str result1)
        result2s (sort-by str result2)
        f (fn [[r1v r1s] [r2v r2s]] (and (= r1v r2v) (is-close? r1s r2s)))]
;    (println "[compare-result] result1s: " result1s)
;    (println "[compare-result] result2s: " result2s)
    (compare-result-sub result1s result2s f)))

(defn- vrp-const-compare-result
  [const env result1 result2]
  (let [result1e (const-evaluate-vrp-result const env result1)
        result2e (const-evaluate-vrp-result const env result2)
        result1s (sort-by str result1e)
        result2s (sort-by str result2e)
        f (fn [[r1v r1renv r1s] [r2v r2renv r2s]] 
            (and (= r1v r2v)
                 (every? true? (map is-close? r1renv r2renv))
                 (is-close? r1s r2s)))]
;    (println "[vrp-const-compare-result] result1s: " result1s)
;    (println "[vrp-const-compare-result] result2s: " result2s)
    (compare-result-sub result1s result2s f)))

(defn- vorp-const-compare-result
  [const env result1 result2]
  (let [result1e (const-evaluate-vorp-result const env result1)
        result2e (const-evaluate-vorp-result const env result2)
        result1s (sort-by str result1e)
        result2s (sort-by str result2e)
        f (fn [[r1v r1obs r1renv r1s] [r2v r2obs r2renv r2s]] 
            (and (= r1v r2v)
                 (every? true? (map is-close? r1obs r2obs))
                 (every? true? (map is-close? r1renv r2renv))
                 (is-close? r1s r2s)))]
    (compare-result-sub result1s result2s f)))

(defn- vorpc-const-compare-result
  [const env result1 result2]
  (let [result1e (const-evaluate-vorp-result const env result1)
        result2e (const-evaluate-vorp-result const env result2)
        f (fn [[v obs renv s c]] (str v " " obs " "  renv " " c " " s))
        result1s (sort-by f result1e)
        result2s (sort-by f result2e)
        g (fn [[r1v r1obs r1renv r1s r1c] [r2v r2obs r2renv r2s r2c]] 
            (and (= r1v r2v)
                 (every? true? (map is-close? r1obs r2obs))
                 (every? true? (map is-close? r1renv r2renv))
                 (is-close? r1s r2s)
                 (= r1c r2c)))]
    ;(println "[vorpc-const-compare-result] result1s: " result1s)
    ;(println "[vorpc-const-compare-result] result2s: " result2s)
    (compare-result-sub result1s result2s g)))

(deftest test-primop
  (testing "primitive operations"
    (is (= (run #{} '(+)) '([0 0.0])))
    (is (= (run #{} '(+ 3)) '([3 0.0])))
    (is (= (run #{} '(+ 3 4)) '([7 0.0])))
    (is (= (run #{} '(+ 3 4 5)) '([12 0.0])))
    (is (= (run #{'x} '(+ 3 (- 1 x) 12 x)) '([16 0.0])))
    (is (= (run #{} '(*)) '([1 0.0])))
    (is (= (run #{} '(* 3)) '([3 0.0])))
    (is (= (run #{} '(* 3 20)) '([60 0.0])))
    (is (= (run #{} '(* 3 20 4)) '([240 0.0])))
    (is (= (run #{'x} '(* 3 (/ 1 x) x)) '([3 0.0])))))

(deftest test0
  (testing "computation involving app and if"
    (is (= (run #{} '((fn f [x y] (if x y 4)) false 43)) '([4 0.0])))))

(deftest test1
  (testing "computation involving app, if and addition"
    (is (= (run #{} '((fn f [x y] (if x (+ y 10) 4)) true 43)) '([53 0.0])))))

(deftest test2
  (testing "computation involving sampling from flip"
    (is (compare-result
          (run #{}
               '(let [x (sample (flip 0.5)) y (sample (flip 0.5))] (and x y)))
          '([false -0.2876820724517809] [true -1.3862943611198906])))))

(deftest test3
  (testing "computation involving sampling from discrete distribution"
    (is (compare-result 
          (run #{}
               '(let [x (sample (discrete (list 1.0 1.0 1.0)))
                      y (sample (discrete (list 1.0 1.0 1.0)))]
                  (+ x y)))
          `([0 ~(Math/log 1/9)]
            [1 ~(Math/log 2/9)]
            [2 ~(Math/log 1/3)]
            [3 ~(Math/log 2/9)]
            [4 ~(Math/log 1/9)])))))

(deftest test4
  (testing "computation involving sampling from categorical distribution"
    (is (compare-result
          (run #{}
               '(let [x (sample (categorical (list [0 1.0] [10 1.0] [20 1.0])))
                      y (sample (categorical (list [0 1.0] [10 1.0] [20 1.0])))]
                  (+ x y)))
          `([0  ~(Math/log 1/9)]
            [10 ~(Math/log 2/9)]
            [20 ~(Math/log 1/3)]
            [30 ~(Math/log 2/9)]
            [40 ~(Math/log 1/9)])))))

(deftest test5
  (testing "computation involving the use of the map function and the categorical distribution"
    (is (compare-result
          (run #{}
               '(let [x (map (fn [y] [(+ y 2) 1.0]) (range 10 16))]
                  (sample (categorical x))))
          `([12 ~(Math/log 1/6)]
            [13 ~(Math/log 1/6)]
            [14 ~(Math/log 1/6)]
            [15 ~(Math/log 1/6)]
            [16 ~(Math/log 1/6)]
            [17 ~(Math/log 1/6)])))))

(deftest test6
  (testing "computation involving sampling from normal distribution"
    (is (vrp-const-compare-result 
          (sample (uniform-continuous -10.0 10.0)) '()
          (vrp-run #{} '(let [x (sample (normal 0.0 1.0))
                              y (sample (normal 0.0 1.0))]
                          (+ x y))) 
          (list ['(+ x y) '([x (normal 0.0 1.0)] [y (normal 0.0 1.0)]) '0.0])))))

(deftest test7
  (testing "computation involving sampling and observing from normal distributions"
    (is (vorp-const-compare-result
          (sample (uniform-continuous -10.0 10.0)) '()
          (vorp-run #{} '(let [x (sample (normal 0.0 1.0))]
                           (observe (normal x 1.0) 10.0)
                           x))
          (list ['x '([(normal x 1.0) 10.0]) '([x (normal 0.0 1.0)]) '0.0])))))

(deftest test8
  (testing "computation invloving conditional statement and sampling from flip and normal"
    (is (vorp-const-compare-result
          (sample (uniform-continuous -10.0 10.0)) 
          (list 'x 0.7 'y 4.5)
          (vorp-run #{'x 'y} 
                    '(let [pos (if (sample (flip x))
                                 (sample (normal 0.0 1.0))
                                 (let [z (sample (normal 0.0 5.0))]
                                   (sample (normal z 5.0))))]
                       (observe (normal pos 5.0) y)
                       pos))
          (list ['b '([(normal b 5.0) y]) '([b (normal 0.0 1.0)]) '(log x)]
                ['b '([(normal b 5.0) y]) '([a (normal 0.0 5.0)] [b (normal a 5.0)]) '(log (- 1.0 x))])))))

(deftest test9
  (testing "part of an aircraft example"
    (is (vrp-const-compare-result
          (sample (uniform-continuous -10.0 10.0)) '()
          (vrp-run #{}
                   '(let [pos (sample (normal 0.0 3.0))
                          num-blips (sample (discrete (list 0.1 0.4 0.5)))
                          blips (map (fn [i] [i (sample (normal pos 1.0))]) (range 0 num-blips))]
                      [pos blips]))
          (list 
            [['x '(list)] '([x (normal 0.0 3.0)]) (log 0.1)]
            [['x '(list [0 b1])] '([x (normal 0.0 3.0)] [b1 (normal x 1.0)]) (log 0.4)]
            [['x '(list [0 b1] [1 b2])] '([x (normal 0.0 3.0)] [b1 (normal x 1.0)] [b2 (normal x 1.0)]) (log 0.5)])))))

(deftest test10
  (testing "part of a Poole's example on lifted inference"
    (is (compare-result
          (run #{}
               '(let [sex (sample (discrete (list 0.49 0.51)))
                      size (if (= sex 0)
                             (sample (discrete (list 0.2 0.3 0.5)))
                             (sample (discrete (list 0.4 0.4 0.2))))
                      hair (sample (discrete (list 0.7 0.28 0.02)))
                      guilty (sample (flip 0.5))]
                  (and (= hair 2) (= size 2) guilty)))
          (list ['true (log 0.0035299999999999997)] ['false (log 0.99647)])))))

(deftest test11
  (testing "part of Pearl's sprinkler example"
    (is (compare-result
          (run #{}
               '(let [is-cloudy (sample (flip 0.7))
                      is-raining (if is-cloudy 
                                   (sample (flip 0.8))
                                   (sample (flip 0.2)))
                      sprinkler (if is-cloudy
                                  (sample (flip 0.1))
                                  (sample (flip 0.5)))
                      p-wet-grass (if (and sprinkler is-raining)
                                    0.99
                                    (if (or sprinkler is-raining) 0.9 0.1))
                      wet-grass (sample (flip p-wet-grass))]
                  (list wet-grass is-cloudy)))
          (list ['(list true true)   (log 0.53424)]
                ['(list true false)  (log 0.1767)]
                ['(list false true)  (log 0.16576)]
                ['(list false false) (log 0.1233)])))))

(deftest test12
  (testing "mixture of three Gaussian models"
    (is (vrp-const-compare-result
          (sample (uniform-continuous -10.0 10.0)) 
          (list 'p '(list 0.1 0.35 0.55) 
                'm1 -5.0  's1 3.0
                'm2  0.0  's2 2.0
                'm3  5.0  's3 4.0)
          (vrp-run #{'p 'm1 's1 'm2 's2 'm3 's3} 
                   '(let [choice (sample (discrete (list (nth p 0) (nth p 1) (nth p 2))))] 
                      (cond 
                        (= choice 0) (sample (normal m1 s1)) 
                        (= choice 1) (sample (normal m2 s2)) 
                        :else        (sample (normal m3 s3))))) 
          (list ['x1 (list ['x1 '(normal m1 s1)]) '(log (nth p 0))]
                ['x2 (list ['x2 '(normal m2 s2)]) '(log (nth p 1))]
                ['x3 (list ['x3 '(normal m3 s3)]) '(log (nth p 2))])))))

(deftest test13
  (testing "a part of a simple markov chain"
    (is (vorpc-const-compare-result
          (sample (uniform-continuous -10.0 10.0)) 
          (list)
          (vorpc-run #{'init-state} 
                     '(let [f-next (fn [prev-state] 
                                     (cond 
                                       (= prev-state 0) (sample (discrete (list 0.1 0.5 0.4))) 
                                       (= prev-state 1) (sample (discrete (list 0.2 0.2 0.6))) 
                                       :else            (sample (discrete (list 0.15 0.15 0.7)))))] 
                        (reduce (fn [cur-state n] (f-next cur-state)) init-state (range 0 20))))
          (list [0 (list) (list) (log 0.15306123) '(= init-state 0)]
                [1 (list) (list) (log 0.21428571) '(= init-state 0)]
                [2 (list) (list) (log 0.63265306) '(= init-state 0)]
                [0 (list) (list) (log 0.15306123) '(= init-state 1)]
                [1 (list) (list) (log 0.21428571) '(= init-state 1)]
                [2 (list) (list) (log 0.63265306) '(= init-state 1)]
                [0 (list) (list) (log 0.15306123) '(and (not= init-state 0) (not= init-state 1))]
                [1 (list) (list) (log 0.21428571) '(and (not= init-state 0) (not= init-state 1))]
                [2 (list) (list) (log 0.63265306) '(and (not= init-state 0) (not= init-state 1))])))))
